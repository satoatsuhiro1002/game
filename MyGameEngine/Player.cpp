#include "Player.h"
#include "Shadow.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

using namespace Model;
using namespace Input;

/////////////////////////////　プレイヤーの準備　//////////////////////////////////

Player::Player(GameObject* parent)
	: GameObject(parent, "Player"),

	//移動速度
	SPEED(0.1f),

	WIDTH(0.3f),//Playerの幅
	HEIGHT(0.6f),//Playerの高さ
	MARGIN(0.11f),//当たり判定の遊び
	BLOCK_SIZE(1.0f),//ブロックのサイズ
	MAX_JUMP(3.0f),//ジャンプの上限
	isJump(false),//ジャンプ中か

	move_(0.01),//Y軸の移動
	gravity_(0.01),//重力

	pStage_(nullptr)//ステージの情報を入れるポインタ
{

}

Player::~Player()
{

}

void Player::Initialize()
{
	//プレイヤーデータのロード
	hModel_ = Model::Load("Assets/Player.fbx");
}

/////////////////////////////　アップデート　//////////////////////////////////
void Player::Update()
{
	//Stageクラスを探す
	//pStage_に探した情報が入る
	if (pStage_ == nullptr)
	{
		pStage_ = (Stage*)Find("Stage");
	}
	
	//左移動
	if (Input::IsKey(DIK_LEFT) || Input::IsKey(DIK_A))
	{
	    transform_.position_.x -= SPEED;
	}

	//右移動
	if (Input::IsKey(DIK_RIGHT)||Input::IsKey(DIK_D))
	{
	    transform_.position_.x += SPEED;
	}

	//プレイヤーの原点は上下で見ると下。左右で見ると真ん中
	//当たったかどうか

	//当たり判定の変数宣言
	int checkX1 , checkX2;
	int checkY1 , checkY2;

	//左
	checkX1 = (int)(transform_.position_.x - WIDTH);
	checkX2 = (int)(transform_.position_.x - WIDTH);
	checkY1 = (int)(transform_.position_.y + (HEIGHT- MARGIN));
	checkY2 = (int)(transform_.position_.y + MARGIN);

	if (pStage_->isCrash(checkX1, checkY1) || pStage_->isCrash(checkX2, checkY2))
	{
		transform_.position_.x = (float)checkX1 + 1.3f;
	}

	//右
	checkX1 = (int)(transform_.position_.x + WIDTH);
	checkX2 = (int)(transform_.position_.x + WIDTH);
	checkY1 = (int)(transform_.position_.y + (HEIGHT - MARGIN));
	checkY2 = (int)(transform_.position_.y + MARGIN);

	if (pStage_->isCrash(checkX1, checkY1) || pStage_->isCrash(checkX2, checkY2))
	{
		transform_.position_.x = (float)checkX1 - 0.3f;
	}


	//上
	checkX1 = (int)(transform_.position_.x + (WIDTH- MARGIN));
	checkX2 = (int)(transform_.position_.x - (WIDTH - MARGIN));
	checkY1 = (int)(transform_.position_.y + HEIGHT);
	checkY2 = (int)(transform_.position_.y + HEIGHT);

	if (pStage_->isCrash(checkX1, checkY1) || pStage_->isCrash(checkX2, checkY2))
	{
		transform_.position_.y = (float)checkY1 - 0.6f;
	}
	else
	{
		//ジャンプ
		if (Input::IsKeyDown(DIK_SPACE) && isJump == false)
		{
			isJump = true;//ジャンプしている

			//gravityの値をマイナスにすることによって今度は上方向に重力がかかるようになる
			transform_.position_.y += move_;
			move_ = -0.2f;
		}
	}


	//下
	checkX1 = (int)(transform_.position_.x + (WIDTH - MARGIN));
	checkX2 = (int)(transform_.position_.x - (WIDTH - MARGIN));
	checkY1 = (int)(transform_.position_.y);
	checkY2 = (int)(transform_.position_.y);

	if (pStage_->isCrash(checkX1, checkY1) || pStage_->isCrash(checkX2, checkY2))
	{
		isJump = false;//下にブロックがあったら今はジャンプしていない

		move_ = 0;

		transform_.position_.y = (float)checkY1 + 1.0f;


	}
	//重力
	//下に何もなかったらどんどん下がる
	else
	{
		pStage_->isButtonCol_ = true;

		transform_.position_.y -= move_;
		
		//ブロックの直系よりは大きくならないようにする
		//→ブロックの直径より値が大きくなるとすり抜ける
		//gravityの値は0.01
		if (move_ < BLOCK_SIZE)
		{
			move_ += gravity_;
		}
	}

}


void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}


void Player::Release()
{

}