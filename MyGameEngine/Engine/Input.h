#pragma once

//インクルード
#include <dInput.h>
#include <DirectXMath.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

using namespace DirectX;

//-----------------------------------------------------------
//DirectInputを使った入力処理
//-----------------------------------------------------------

namespace Input
{
	void Initialize(HWND hWnd);
	void Update();
	///////////////////////////　キーボード　//////////////////////////////////

	//キーが押されているか調べる
	//引数：keyCode	調べたいキーのコード
	// (dinput参照)
	//戻値：押されていればtrue
	bool IsKey(int keyCode);

	//キーを今押したか調べる（押しっぱなしは無効）
	//引数：keyCode	調べたいキーのコード
	// (dinput参照)
	//戻値：押した瞬間だったらtrue
	bool IsKeyDown(int keyCode);

	//キーを今放したか調べる
	//引数：keyCode	調べたいキーのコード
	//  (dinput参照)
	//戻値：放した瞬間だったらtrue
	bool IsKeyUp(int keyCode);

	///////////////////////////　マウス　//////////////////////////////////

	//1：マウスのボタンが押されているか調べる
	//引数 0：左ボタン
	//引数 1：右ボタン
	//引数 2：中央ボタン	
	//戻値：押されていればtrue
	bool IsMouseButton(int buttonCode);

	//2：マウスのボタンを今押したか調べる（押しっぱなしは無効）
	//引数 0：左ボタン
	//引数 1：右ボタン
	//引数 2：中央ボタン
	//戻値：押した瞬間だったらtrue
	bool IsMouseButtonDown(int buttonCode);

	//3：マウスのボタンを今放したか調べる
	//引数 0：左ボタン
	//引数 1：右ボタン
	//引数 2：中央ボタン
	//戻値：放した瞬間だったらtrue
	bool IsMouseButtonUp(int buttonCode);

	//マウスカーソルの位置を取得
	//戻値：マウスカーソルの位置
    XMVECTOR GetMousePosition();

	//マウスカーソルの位置をセット
	//引数：マウスカーソルの位置
	void SetMousePosition(float x, float y);

	//6：そのフレームでのマウスの移動量を取得
	//戻値：X,Y マウスの移動量　／　Z,ホイールの回転量
	XMFLOAT3 GetMouseMove();

	

	///////////////////////////　解放　//////////////////////////////////

	//解放
	void Release();
};

