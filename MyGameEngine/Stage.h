#pragma once
#include "Engine/GameObject.h"
#include <Windows.h>

class Player;
class Shadow;


//ステージを管理するクラス
class Stage : public GameObject
{
    int width  = 20; //ステージの幅、Xにあたる
    int height = 12; //ステージの高さ、Yにあたる
    int stageNam;    //ステージ選択（テスト用）
    int hModel_[99]; //モデルデータが入ってる

    //x->横軸
    //戻値：１〜２０までの数値
    int mapX;
    
    //y->縦軸
    //戻値：１〜１２までの数値
    int mapY;

    Player* pPlayer_;
    Shadow* pShadow[6];

public:
    bool isButtonCol_;
    
    //ステージに配置されているブロックの場所を記憶している。
    //[横][縦]で入っている。
    //戻値：Excelファイルのステージ構成の数値（０なら何も入ってない（Stage.cpp参照））
    int map_[30][12];

    //プレイヤーの場所を記憶している
    XMFLOAT3 stertPos;

public:
    //コンストラクタ
    Stage(GameObject* parent);

    //デストラクタ
    ~Stage();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //ぶつかるかどうか
    bool isCrash(int x , int y);

};

