#include "Stage.h"
#include "Engine/Model.h"
#include "Player.h"
#include "Shadow.h"
#include "Engine/CsvReader.h"
#include "Engine/Input.h"

using namespace Input;

/////////////////////////////　コンストラクタ　//////////////////////////////////
Stage::Stage(GameObject* parent)
    :GameObject(parent, "Stage"),
    pPlayer_(nullptr)
{
}

/////////////////////////////　デストラクタ　//////////////////////////////////
Stage::~Stage()
{
}

/////////////////////////////　初 期 化　//////////////////////////////////
void Stage::Initialize()
{
    //ステージモデル一覧（Player以外）


    //配列最大98種
    //hModel_[99]にはPlayerが入るため、空白にする
    //Excelで作るときはモデルの数字をそのまま当てはめればOK!
    //(ExcelファイルはMyGameEngine->Assetsフォルダ内にある。)
    //色の並び順は仕様書順に並べる


    //01〜->初期フレーム（色なしフレーム）
    hModel_[1] = Model::Load("Assets/Frame_Box.fbx");
    hModel_[2] = Model::Load("Assets/Frame_Triangle.fbx");
    hModel_[3] = Model::Load("Assets/Frame_Circle.fbx");

    //10〜->四角フレーム（色付き）
    //識別名:Frame_Box_
    hModel_[10] = Model::Load("Assets/Frame_Box_Green.fbx");
    hModel_[11] = Model::Load("Assets/Frame_Box_Yellow.fbx");
    hModel_[12] = Model::Load("Assets/Frame_Box_Orange.fbx");
    hModel_[13] = Model::Load("Assets/Frame_Box_Red.fbx");

    //20〜->三角フレーム（色付き）
    //識別名:Frame_Triangle_
    hModel_[20] = Model::Load("Assets/Frame_Triangle_Green.fbx");
    hModel_[21] = Model::Load("Assets/Frame_Triangle_Yellow.fbx");
    hModel_[22] = Model::Load("Assets/Frame_Triangle_Orange.fbx");
    hModel_[23] = Model::Load("Assets/Frame_Triangle_Red.fbx");

    //30〜->丸フレーム（色付き）
    //識別名:Frame_Circle_
    hModel_[30] = Model::Load("Assets/Frame_Circle_Green.fbx");
    hModel_[31] = Model::Load("Assets/Frame_Circle_Yellow.fbx"); 
    hModel_[32] = Model::Load("Assets/Frame_Circle_Orange.fbx");
    hModel_[33] = Model::Load("Assets/Frame_Circle_Red.fbx");

    //40〜->その他（エンジン作成した時のブロック（仮置きとかのテスト用））
    //識別名:Normal_
    hModel_[40] = Model::Load("Assets/Normal_Blue.fbx");//仮のゴールブロック
    hModel_[41] = Model::Load("Assets/Normal_Green.fbx");
    hModel_[42] = Model::Load("Assets/Normal_Yellow.fbx");

    //-----------------------------------------------------------------
    //50〜→ギミック関係(の予定)
    //β版にて制作予定
    //60以降→未定
    //-----------------------------------------------------------------


    //ステージ生成
    //Csvファイルの読み込み
    CsvReader csv;
    
    //ステージ選択
    //プレイしたいステージ選択（テスト用含む）
    stageNam = 2;

    //ステージ分岐
    switch (stageNam)
    {
        case 1:  csv.Load("Assets/Open.csv");   break;//アルファ版公開用ステージ
        case 2:  csv.Load("Assets/Test.csv");   break;//テストステージ
        case 3:  csv.Load("Assets/Stage1.csv"); break;//正規ステージ（の予定）
        default: csv.Load("Assets/Open.csv");   break;//その他の時用
    }
    
    //Excelファイルのマスの行
    //T→２０マス　

    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < (height - 1); y++)
        {
            //エクセルだとyの値が逆なので（高さー１）の値を入れる（今回は高さが１２なので、１１を入れている+）
            map_[x][y] = csv.GetValue(x, 11 - y); 

            if (map_[x][y] == 99)//プレイヤーの場所を調べる
            {
                //プレイヤーの生成
                Player* pPlayer = (Player*)Instantiate<Player>(this->pParent_);

                //プレイヤーの位置決定
                pPlayer->transform_.position_.x = x;
                pPlayer->transform_.position_.y = y; 

                //プレイヤーの初期位置を記憶する
                stertPos = pPlayer->transform_.position_;
            }
        }
    }
}

/////////////////////////////　更 新　//////////////////////////////////
void Stage::Update()
{
    if (pPlayer_ == nullptr)
    {
        pPlayer_ = (Player*)Find("Player");
    }

    //マウスの右クリックを押したとき
    //現状ｘは２７ブロック目まで映ってる
    if (IsMouseButton(0))
    {
        //1：ベクトルを扱う箱を用意して、マウスの座標を取得する

        //マウスの座標を代入する
        //戻値：マウスの座標（vector型）
        XMVECTOR mouse;
        SetMousePosition;               //マウスの座標を確認する
        mouse = GetMousePosition();     //マウスの座標を取得して、mouseに代入する

        //2：float型に直す

        //マウスの座標（float型）が入っている
        //戻値：マウスの座標（float型）
        XMFLOAT3 mapPoint;
        XMStoreFloat3(&mapPoint,mouse); //vector型をfloat型に変える

        //3：縦横それぞれの座標をmap_配列にあうように計算して、代入する
        mapX = (mapPoint.x / 29.6f)-0.5f;  //横軸の計算をする
        mapY = (mapPoint.y / 50.0f);     //縦軸の計算をする（作成に時間かかってる）

        //４：実際にブロックを置き換える
        
        //クリックした先にゴールブロック以外のブロックが置かれている時
        if (map_[mapX][mapY] != 0&& map_[mapX][mapY] != 40)
        {
            //クリックしたブロックを１０番のブロックに置き換える（ステージモデル一覧参照）
            map_[mapX][mapY] = 10;
        }

        //５：ステージをロードしなおす
        Stage::Draw();
    };
}

/////////////////////////////　描 画　//////////////////////////////////
void Stage::Draw()
{
    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            if (map_[x][y] == 0 || map_[x][y] == 99)
            {
                continue;
            }
            int type = map_[x][y];
            Transform trans;
            trans.position_.x = x;
            trans.position_.y = y;
            trans.Calclation();

            //ここで例外処理が出る場合
            //１：Excelファイルにデータが入ってない
            Model::SetTransform(hModel_[type], trans);
            Model::Draw(hModel_[type]);
            int a = 0;
        }
    }
}

/////////////////////////////　開放　//////////////////////////////////
void Stage::Release()
{
}

/////////////////////////////　障害物判定　//////////////////////////////////
//そのマスに障害物があるかどうか
//戻り値、何かあるtrue,何もないfalse
bool Stage::isCrash(int x, int y)
{
    //配列に1が入っていれば通れない
    if (map_[x][y] == 0 || map_[x][y] == 99)
    {
        return false;
    }
    else
    {
        return true;
    }
}