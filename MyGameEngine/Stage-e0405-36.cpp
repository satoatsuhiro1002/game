#include "Stage.h"
#include "Engine/Model.h"
#include "Player.h"
#include "Shadow.h"
#include "Engine/CsvReader.h"
#include "Engine/Input.h"

using namespace Input;

/////////////////////////////　コンストラクタ　//////////////////////////////////
Stage::Stage(GameObject* parent)
    :GameObject(parent, "Stage"),
    pPlayer_(nullptr),
    shadowCount_(0)
{
}

/////////////////////////////　デストラクタ　//////////////////////////////////
Stage::~Stage()
{
}

/////////////////////////////　初 期 化　//////////////////////////////////
void Stage::Initialize()
{
    //ステージモデル一覧（Player以外）


    //配列最大98種
    //hModel_[99]にはPlayerが入るため、空白にする
    //Excelで作るときはモデルの数字をそのまま当てはめればOK!
    //(ExcelファイルはMyGameEngine->Assetsフォルダ内にある。)
    //色の並び順はABC順に並べる



    //01〜->初期フレーム（色なしフレーム）
    hModel_[1] = Model::Load("Assets/Frame_Box.fbx");
    hModel_[2] = Model::Load("Assets/Frame_Triangle.fbx");
    hModel_[3] = Model::Load("Assets/Frame_Circle.fbx");

    //10〜->四角フレーム（色付き）
    //識別名:Frame_Box_
    hModel_[10] = Model::Load("Assets/Frame_Box_Yellow.fbx");
    hModel_[11] = Model::Load("Assets/Frame_Box_Green.fbx");
    hModel_[12] = Model::Load("Assets/Frame_Box_Orange.fbx");
    hModel_[13] = Model::Load("Assets/Frame_Box_Red.fbx");

    //20〜->三角フレーム（色付き）
    //識別名:Frame_Triangle_
    hModel_[20] = Model::Load("Assets/Frame_Triangle_Yellow.fbx");
    hModel_[21] = Model::Load("Assets/Frame_Triangle_Green.fbx");
    hModel_[22] = Model::Load("Assets/Frame_Triangle_Orange.fbx");
    hModel_[23] = Model::Load("Assets/Frame_Triangle_Red.fbx");

    //30〜->丸フレーム（色付き）
    //識別名:Frame_Circle_
    hModel_[30] = Model::Load("Assets/Frame_Circle_Yellow.fbx");
    hModel_[31] = Model::Load("Assets/Frame_Circle_Green.fbx"); 
    hModel_[32] = Model::Load("Assets/Frame_Circle_Orange.fbx");
    hModel_[33] = Model::Load("Assets/Frame_Circle_Red.fbx");

    //40〜->その他（先生が用意したブロック）
    //識別名:Normal_
    hModel_[40] = Model::Load("Assets/Normal_Blue.fbx");
    hModel_[41] = Model::Load("Assets/Normal_Green.fbx");
    hModel_[42] = Model::Load("Assets/Normal_Yellow.fbx");

    //-----------------------------------------------------------------
    //50〜→ギミック関係(の予定)
    //β版にて制作予定
    //60以降→未定
    //-----------------------------------------------------------------


    //ステージ生成
    //Csvファイルの読み込み
    CsvReader csv;
    csv.Load("Assets/Stage1.csv");
    
    //Excelファイルのマスの場所
    //T→２０マス目
    for (int x = 0; x < 20; x++)
    {
        for (int y = 0; y < 12; y++)
        {
            //エクセルだとyの値が逆なので（高さー１）の値を入れる（今回は高さが１２なので、１１を入れている）
            map_[x][y] = csv.GetValue(x, 11 - y); 
            if (map_[x][y] == 99)//プレイヤーの場所を調べる
            {
                //プレイヤーの生成
                Player* pPlayer = (Player*)Instantiate<Player>(this->pParent_);

                //プレイヤーの位置決定
                pPlayer->transform_.position_.x = x;
                pPlayer->transform_.position_.y = y; 

                //プレイヤーの初期位置を記憶する
                stertPos = pPlayer->transform_.position_;
            }
        }
    }
}

/////////////////////////////　更 新　//////////////////////////////////
void Stage::Update()
{
    if (pPlayer_ == nullptr)
    {
        pPlayer_ = (Player*)Find("Player");
    }

    //マウスの右クリックを押したとき
    //現状ｘは２７ブロック目まで映ってる
    if (IsMouseButtonDown(0))
    {
        SetMousePosition;
        mouse = GetMousePosition();

        mapX = 30/mouse.x;
        mapY = 12/mouse.y;
        map_[mapX][mapY] = 10;
        Stage::Draw();
    };
}

/////////////////////////////　描 画　//////////////////////////////////
void Stage::Draw()
{
    for (int x = 0; x < 30; x++)
    {
        for (int y = 0; y < 12; y++)
        {
            if (map_[x][y] == 0 || map_[x][y] == 99)
            {
                continue;
            }
            int type = map_[x][y];
            Transform trans;
            trans.position_.x = x;
            trans.position_.y = y;
            trans.Calclation();

            Model::SetTransform(hModel_[type], trans);
            Model::Draw(hModel_[type]);
            int a = 0;
        }
    }
}

/////////////////////////////　開放　//////////////////////////////////
void Stage::Release()
{
}

/////////////////////////////　障害物判定　//////////////////////////////////
//そのマスに障害物があるかどうか
//戻り値、何かあるtrue,何もないfalse
bool Stage::isCrash(int x, int y)
{
    //配列に1が入っていれば通れない
    if (map_[x][y] == 0 || map_[x][y] == 99)
    {
        return false;
    }
    else
    {
        return true;
    }
}
/////////////////////////////　ブロック移動　//////////////////////////////////
void Stage::MoveBlock()
{

}
